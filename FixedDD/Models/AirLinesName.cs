﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FixedDD.Models
{
    [Table("AirLinesName")]
    public class AirLinesName
    {
        public int id { get; set; }

        public string AL_Code { get; set; }

        public string AL_Name { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}