﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace FixedDD.Models
{
    public class Global
    {
        #region get Connecton
        public static string GetConnection
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            }
        }
        #endregion

        #region get Source Data
        public static DataTable GetSourceData
        {
            get
            {
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand cmd = new SqlCommand("SurceListProc", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }
        #endregion

        #region get Source Data Details
        public static DataTable GetSourceDataDetails(string val)
        {
            try
            {
                List<string> sc = val.Split(',').ToList();
                SqlConnection con = new SqlConnection(GetConnection);
                SqlCommand cmd = new SqlCommand("GetSuorceDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@way", sc[0]);
                cmd.Parameters.AddWithValue("@Dest", sc[1]);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }
        #endregion
    }
}