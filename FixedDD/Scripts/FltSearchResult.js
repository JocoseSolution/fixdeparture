﻿
$(document).ready(function () {
    $("#AirLineName").autocomplete({
        source: function (request, response) {

            $.ajax({

                url: UrlBase + "AirLinesNames/GetAirLinesName",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.AL_Name + ',' + '(' + item.AL_Code + ')', value: item.AL_Name + ',' + '(' + item.AL_Code + ')' };
                    }))

                }
            })
        },
        minLength: 2,
        messages: {
            noResults: "", results: ""
        }

    });
})
$(document).ready(function () {
    $("#Rt_AirLineName").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: UrlBase + "AirLinesNames/GetAirLinesName",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.AL_Name + ',' + '(' + item.AL_Code + ')', value: item.AL_Name + ',' + '(' + item.AL_Code + ')' };
                    }))

                }
            })
        },
        minLength: 2,
        messages: {
            noResults: "", results: ""
        }

    });
})

$(document).ready(function () {
    $("#OrgDestFrom").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: UrlBase+"FlightSearchResults/GetCityData",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.CityName + ',' + item.AirportName + ',' + '(' + item.AirportCode + ')', value: item.CityName + ',' + item.AirportName + ',' + '(' + item.AirportCode + ')' };
                    }))

                }
            })
        },
        minLength: 3,
        messages: {
            noResults: "", results: ""
        }

    });
})


$(document).ready(function ()
{
    $("#OrgDestTo").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: UrlBase + "FlightSearchResults/GetCityData",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.CityName + ',' + item.AirportName + ',' + '(' + item.AirportCode + ')', value: item.CityName + ',' + item.AirportName + ',' + '(' + item.AirportCode + ')' };
                    }))

                }
            })
        },
        minLength: 3,
        messages: {
            noResults: "", results: ""
        }

    });
})


function SetTimevalue()
{
    var timepickers = $('.timepicker').wickedpicker();
    console.log(timepickers.wickedpicker('time', 1));
}

function SetTimevaluesecond() {
    var timepickers = $('.timepicker1').wickedpicker();
    console.log(timepickers.wickedpicker('time', 1));
}
function SetTimevalueThird() {
    var timepickers = $('.timepicker').wickedpicker();
    console.log(timepickers.wickedpicker('time', 1));
}
function SetTimevalueFourth() {
    var timepickers = $('.timepicker1').wickedpicker();
    console.log(timepickers.wickedpicker('time', 1));
}
function TotalSeat() {

    var txtFirstNumberValue = document.getElementById('Total_Seats').value;
    var txtSecondNumberValue = document.getElementById('Used_Seat').value;
    var result = parseFloat(txtFirstNumberValue) - parseFloat(txtSecondNumberValue);
    if (!isNaN(result)) {
        document.getElementById('Avl_Seat').value = result;
    }
  
}


function sum()
{
   
    var txtFirstNumberValue = document.getElementById('Basicfare').value;
    var txtSecondNumberValue = document.getElementById('YQ').value;
    var txtThirdNumberValue = document.getElementById('YR').value;
    var txtFourNumberValue = document.getElementById('WO').value;
    var txtFiveNumberValue = document.getElementById('OT').value;   
    var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue) + parseFloat(txtThirdNumberValue) + parseFloat(txtFourNumberValue) + parseFloat(txtFiveNumberValue);
    if (!isNaN(result))
    {
        document.getElementById('GROSS_Total').value = result;
    }  
   
}

function addition() {

    var markup2 = document.getElementById('Markup').value;
    var Gtotal = document.getElementById('GROSS_Total').value;

    var mark = (parseFloat(markup2) * parseFloat(Gtotal)) / 100;
    var final = parseFloat(mark) + parseFloat(Gtotal);
    if (!isNaN(final)) {
        document.getElementById('Grand_Total').value = final;
    }
}

function flat() {
    var Gtotal = document.getElementById('GROSS_Total').value;
    var admin = document.getElementById('Admin_Markup').value;
    var total_flat = parseFloat(Gtotal) + parseFloat(admin);
    if (!isNaN(total_flat)) {
        document.getElementById('Grand_Total').value = total_flat;
    }
}

function FeedData(Selected) {
    debugger

    var markup = document.getElementById('txt');
    var admin_Markup = document.getElementById('txt2');
    if (Selected == '--Selcet One--' || Selected == 'Markup %')
        markup.style.display = 'block';

    else
        markup.style.display = 'none';
    if (Selected == '--Selcet One--' || Selected == 'Markup %')
        admin_Markup.style.display = 'none';
    else
        admin_Markup.style.display = 'block'
    var Gtotal = document.getElementById('GROSS_Total').value;
    var admin = document.getElementById('Admin_Markup').value;
    if (Selected == '--Selcet One--' || Selected == 'Flat') {
        var Total = parseFloat(Gtotal) + parseFloat(admin);
    }
    if (!isNaN(Total)) {
        document.getElementById('Grand_Total').value = Total;
    }
    //var markup2 = document.getElementById('Markup').value;
    //if (Selected == '--Selcet One--' || Selected == 'others') {
    //    var mark = (parseFloat(markup2) * parseFloat(Gtotal)) / 100;
    //    var final = parseFloat(mark) + parseFloat(Gtotal);
        
    //}
    //if (!isNaN(final)) {
    //    document.getElementById('Grand_Total').value = final;
    //}
}

function Validation_Pax() {
    debugger;
    var txtFirstNumberValue = document.getElementById('infant').value;
    var txtSecondNumberValue = document.getElementById('Child').value;
    var txtThirdNumberValue = document.getElementById('Adult').value;  
    var result = parseFloat(txtFirstNumberValue) + parseFloat(txtSecondNumberValue) + parseFloat(txtThirdNumberValue);
    if (!isNaN(result)) {
        if (result > 9) {
            alert('Number Of Infant Should Be Less Than Or Equal To Number Of Adult');
        }
        else {
            ShowDetails();
        }
    }

}


$(function () {
    $('input[name="Triptype"]').change(function () {
        if ($(this).val() == 'Round Trip') {
            $('#tmt').show();
        }
        else {
            $('#tmt').hide();
        }
    });
});



$(document).ready(function () {
    $('#Arrival_Date').datepicker({
        dateFormat: "dd/mm/yy",
        showStatus: true,
        showWeeks: true,
        currentText: 'Now',
        autoSize: true,
        gotoCurrent: true,
        minDate: -0,  
        showAnim: 'blind',
        highlightWeek: true
    });
});


$(document).ready(function () {
    $('#Departure_Date').datepicker({
        dateFormat: "dd/mm/yy",
        showStatus: true,
        showWeeks: true,
        currentText: 'Now',
        autoSize: true,
        gotoCurrent: true,
        minDate: -0,  
        showAnim: 'blind',
        highlightWeek: true
    });
});

$(document).ready(function () {
    $('#valid_Till').datepicker({
        dateFormat: "dd/mm/yy",
        showStatus: true,
        showWeeks: true,
        currentText: 'Now',
        autoSize: true,
        gotoCurrent: true,
        minDate: -0,  
        showAnim: 'blind',
        highlightWeek: true
    });
});
$(document).ready(function () {
    $('#Rt_Departure_Date').datepicker({
        dateFormat: "dd/mm/yy",
        showStatus: true,
        showWeeks: true,
        currentText: 'Now',
        autoSize: true,
        gotoCurrent: true,
        minDate: -0,
        showAnim: 'blind',
        highlightWeek: true
    });
});
$(document).ready(function () {
    $('#Rt_Arrival_Date').datepicker({
        dateFormat: "dd/mm/yy",
        showStatus: true,
        showWeeks: true,
        currentText: 'Now',
        autoSize: true,
        gotoCurrent: true,
        minDate: -0,
        showAnim: 'blind',
        highlightWeek: true
    });
});

$('.dateAllFormate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true
});






