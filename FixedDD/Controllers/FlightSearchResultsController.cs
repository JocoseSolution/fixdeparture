﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using FixedDD.Helper;
using FixedDD.Models;

namespace FixedDD.Controllers
{
    public class FlightSearchResultsController : Controller
    {
        private myAmdDB db = new myAmdDB();

        public Page Page { get; private set; }

        public ActionResult AddSeat(string id, string seat2)
        {
            FixdDepModel model = new FixdDepModel();
            model.Id = id;
            model.Avl_Seat = seat2;
            return View(model);
        }
        [HttpPost]
        public ActionResult AddSeat(FixdDepModel model, string submitButton)
        {
            int toltalseat = 0;
            if (submitButton == "Add Seat")
            {
                toltalseat = Convert.ToInt32(model.Avl_Seat) + Convert.ToInt32(model.Seat);
            }
            else
            {
                toltalseat = Convert.ToInt32(model.Avl_Seat) - Convert.ToInt32(model.Seat);
            }
            string fieldswithvalue = "Avl_Seat = " + toltalseat + " ,Total_Seats="+ toltalseat;
            string tablename = "FlightSearchResults";
            string wherecondition = "Id = " + model.Id;
            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            if (IsSuccess)
            {
                if (submitButton == "Add Seat")
                {
                    TempData["Msg"] = "Seat added successfully";
                }
                else
                {
                    TempData["Msg"] = "Seat deducted successfully";
                }
            }

            return View(model);
        }

        public ActionResult MeangeFare(string id)
        {
            FixdDepModel model = new FixdDepModel();
            model.Id = id;
            return View(model);
        }
        [HttpPost]
        public ActionResult MeangeFare(FixdDepModel model)
        {
            string fieldswithvalue = "Grand_Total = " + model.Grand_total + "";
            string tablename = "FlightSearchResults";
            string wherecondition = "Id = " + model.Id;
            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            if (IsSuccess)
            {
                TempData["Msg"] = "Fare Update";
            }
            return View(model);
        }
        public ActionResult TicketReport(FlightTicketFilter filter)
        {
            FlightTicketReportModel model = new FlightTicketReportModel();
            try
            {
                int totalcount = 0;
                filter.Status = StatusClass.Ticketed.ToString();
                filter.UserType = "Supp";
                filter.Suppliorid = Session["userid"].ToString();
                model.TicketReportlist = AccountHelper.GetFixDepTicketReports(filter, ref totalcount);
                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult LedgerReport(FlightLedgerFilter filter)
        {
            FlightLedgerReportModel model = new FlightLedgerReportModel();
            try
            {
                int totalcount = 0;
                //filter.Status = StatusClass.Ticketed.ToString();
                filter.UserType = "AGENT";
                filter.Loginid = Session["userid"].ToString();
                filter.SearchType = "Own";
                model.LedgerReportlist = AccountHelper.GetFixDepLedgerReports(filter, ref totalcount);
                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult UpdatPnr(string OrderId)
        {
            FixdDepHoldBookingModel model = new FixdDepHoldBookingModel();
            model.OrderId = OrderId;
            return View(model);
        }
        [HttpPost]
        public ActionResult UpdatPnr(FixdDepHoldBookingModel model)
        {
            string fieldswithvalue = "GdsPnr = '" + model.GdsPnr + "',Status='Ticketed',Remark='" + model.Remark + "',UpdateDate=GETDATE()";
            string tablename = "FltHeader";
            string wherecondition = "OrderId ='" + model.OrderId + "'";
            bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
            if (IsSuccess)
            {
                TempData["Msg"] = "Pnr Update";
            }
            return View(model);
        }
        public ActionResult HoldPnrRequest(FlightTicketFilter filter)
        {
            FlightTicketReportModel model = new FlightTicketReportModel();
            try
            {
                int totalcount = 0;
                filter.Suppliorid = Session["userid"].ToString();
                model.TicketReportlist = AccountHelper.GetFlightHoldTicketReports(filter, ref totalcount);
                model.Totalcount = totalcount;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult Supplerlogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Supplerlogin(Account model)
        {
            string message = string.Empty;
            DataTable dtAgentDel = AccountHelper.GetAgentRegisterDetails(model.UserId, model.Password);

            if (dtAgentDel != null && dtAgentDel.Rows.Count > 0)
            {
                string ColName = string.Empty;
                if (dtAgentDel.Columns.Contains("UID"))
                {
                    ColName = "UID";
                }
                if (dtAgentDel.Columns.Contains("PWD"))
                {
                    ColName = "PWD";
                }
                if (dtAgentDel.Columns.Contains("Password"))
                {
                    ColName = string.Empty;
                }


                if (string.IsNullOrEmpty(ColName))
                {
                    bool IsSupplier = Convert.ToBoolean(dtAgentDel.Rows[0]["IsSupplier"].ToString());
                    if (IsSupplier)
                    {
                        string R1 = Encrypt(model.UserId + "-" + model.Password + "-" + DateTime.Now.ToString("yyMMddHHmmssff"));
                        if (AccountHelper.InsertQuery(R1))
                        {
                            Session["isExecutive"] = 1;
                            return RedirectToAction("Index", new { R1 = R1 });
                        }
                    }
                    else
                    {
                        TempData["Message"] = "You are not authoried to access this service.";
                    }
                }
                else
                {
                    TempData["Message"] = dtAgentDel.Rows[0][ColName].ToString();
                }
            }


            return View();
        }

        public ActionResult Flight_Search(string Sector)
        {

            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }
            ViewBag.secor = Sector;
            return View();
        }


        public ActionResult Sector()
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }
            return View();
        }

        public ActionResult Logout()
        {
            string redirectUrl = string.Empty;

            if (Session["isExecutive"] != null)
            {
                redirectUrl = "/";
            }
            else
            {
                redirectUrl = "/";
            }

            Session.Abandon();
            Session.Clear();
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect(redirectUrl);
            }
            return View();
        }

        public ActionResult Backhome()
        {
            string R2 = "";
            if (Convert.ToString(Session["userid"]) != "")
            {

                R2 = Encrypt(Session["userid"] + "-" + Session["Code"]);

            }
            //http://support.ghaitravels.com/Login.aspx?R2=" + R2 + "
            return Json("/");

        }
        // GET: FlightSearchResults
        public ActionResult Index(int? page, string from, string to)
        {
            string matchkey = "";
            IQueryable<FlightSearchResults> stdList;
            int totalskip;
            string UserID = "";
            string pwd = "";
            string R1 = "";
            string rr = "";

            bool no = false;


            if (Request.QueryString["R1"] != null)
                matchkey = QueryKeyI(Request.QueryString["R1"]);


            //  Response.Write("<script>alert('" + rr + "');</script>");



            if (matchkey == "NO")
            {
                no = true;

                if (Convert.ToString(Session["query"]) != null)
                {
                    try
                    {
                        if (Request.QueryString["R1"] != null && no == false)
                        {

                            if (Convert.ToString(Session["R1"]) == Convert.ToString(Request.QueryString["R1"]))
                            {
                                R1 = Session["R1"].ToString();
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }
                            else
                            {
                                R1 = Decrypt(Request.QueryString["R1"].ToString());
                                Session["R1"] = R1;
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }

                        }
                        else if (Request.QueryString["R1"] == null && no == false)
                        {
                            if (Convert.ToString(Session["R1"]) == "")
                            {
                                //Redirect Action
                            }
                            else
                            {
                                R1 = Session["R1"].ToString();
                                UserID = R1.Split('-')[0];
                                pwd = R1.Split('-')[1];
                            }
                        }
                        else
                        {
                            // UserID = Decrypt(Request.QueryString["UserID"].ToString());
                            //pwd = Decrypt(Request.QueryString["Code"].ToString());
                        }




                        int temp = validUser(UserID, pwd);

                        if (Session["isExecutive"] != null)
                        {
                            temp = 1;
                        }

                        if (temp > 0)
                        {
                            if (Convert.ToString(Session["R1"]) == "")
                            {
                                Session["userid"] = UserID;
                                Session["Code"] = pwd;
                                Session["R1"] = UserID + "-" + pwd;
                                Session["query"] = UserID;
                                return RedirectToAction("Sector");
                            }
                            else
                            {
                                Session["userid"] = UserID;
                                Session["Code"] = pwd;

                            }




                        }
                        else
                        {

                            Response.Redirect("/");

                        }

                    }
                    catch
                    {
                        Response.Redirect("/");

                    }

                }
                else
                {
                    Response.Redirect("/");
                }
            }
            else if (matchkey == "YES" || Convert.ToString(Session["query"]) != null)
            {


                try
                {
                    if (Request.QueryString["R1"] != null && no == false)
                    {

                        if (Convert.ToString(Session["R1"]) == Convert.ToString(Request.QueryString["R1"]))
                        {
                            R1 = Session["R1"].ToString();
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }
                        else
                        {
                            R1 = Decrypt(Request.QueryString["R1"].ToString());
                            Session["R1"] = R1;
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }

                    }
                    else if (Request.QueryString["R1"] == null && no == false)
                    {
                        if (Convert.ToString(Session["R1"]) == "")
                        {
                            //Redirect Action
                        }
                        else
                        {
                            R1 = Session["R1"].ToString();
                            UserID = R1.Split('-')[0];
                            pwd = R1.Split('-')[1];
                        }
                    }
                    else
                    {
                        // UserID = Decrypt(Request.QueryString["UserID"].ToString());
                        //pwd = Decrypt(Request.QueryString["Code"].ToString());
                    }




                    int temp = validUser(UserID, pwd);
                    if (Session["isExecutive"] != null)
                    {
                        temp = 1;
                    }
                    if (temp > 0)
                    {
                        if (Convert.ToString(Session["R1"]) == "")
                        {
                            Session["userid"] = UserID;
                            Session["Code"] = pwd;
                            Session["R1"] = UserID + "-" + pwd;
                            Session["query"] = UserID;
                            return RedirectToAction("Sector");
                        }
                        else
                        {
                            Session["userid"] = UserID;
                            Session["Code"] = pwd;

                        }




                    }
                    else
                    {

                        Response.Redirect("/");

                    }

                }
                catch
                {
                    Response.Redirect("/");

                }
            }

            GetIndex(page, from, to, out stdList, out totalskip);
            return View(stdList.Skip(totalskip).Take(10));

            //GetIndex(page, search, out stdList, out totalskip);
            //return View(stdList);
        }

        protected int validUser(string userID, string PWD)
        {

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataAdapter adp = new SqlDataAdapter();


            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("UserLogin_PP");
            cmd.Connection = con;
            con.Open();

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Usename", userID);
            cmd.Parameters.AddWithValue("@Password", PWD);


            adp.SelectCommand = cmd;
            int temp = adp.Fill(ds);
            cmd.Dispose();
            return temp;

        }

        protected string QueryKeyI(string QueryKey)
        {
            string ReturnQUERY = "";
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            SqlDataAdapter adp = new SqlDataAdapter();


            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("SP_QueryKey");
            cmd.Connection = con;
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@QueryKey", QueryKey.Replace(" ", "+"));
            cmd.Parameters.AddWithValue("@Action", "S");
            ReturnQUERY = Convert.ToString(cmd.ExecuteScalar());
            cmd.Dispose();
            return ReturnQUERY;

        }

        private string Decrypt(string cipherText)
        {

            cipherText = cipherText.Replace(" ", "+");
            string EncryptionKey = "MAKV2SPBNI99520";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {

                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99520";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public PartialViewResult NextResult(int? page, string from, string to)
        {
            IQueryable<FlightSearchResults> stdList;
            int totalskip;
            GetIndex(page, from, to, out stdList, out totalskip);
            return PartialView(stdList.Skip(totalskip).Take(10));
            //return PartialView(stdList);
        }
        private void GetIndex(int? page, string from, string to, out IQueryable<FlightSearchResults> stdList, out int totalskip)
        {
            string UserID = Session["userid"].ToString();
            int pageno = page ?? 1;
            totalskip = (pageno - 1) * 10;
            if (!string.IsNullOrEmpty(from))
            {
                stdList = db.FlightSearchResults.Where(b => b.OrgDestFrom.Contains(from) && b.CreatedByUserid == UserID).OrderByDescending(b => b.id);
            }
            if (!string.IsNullOrEmpty(from) && !string.IsNullOrEmpty(to))
            {
                stdList = db.FlightSearchResults.Where(b => b.OrgDestFrom.Contains(from) && b.OrgDestTo.Contains(to) && b.CreatedByUserid == UserID).OrderByDescending(b => b.id);
            }
            else
            {
                stdList = db.FlightSearchResults.Where(b => b.CreatedByUserid == UserID).OrderBy(b => b.id);
            }
            ViewBag.from = from;
            ViewBag.To = to;
            //stdList = db.FlightSearchResults.OrderBy(b => b.id);
            ViewBag.totalrecord = stdList.Count();
        }

        // GET: FlightSearchResults/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            if (flightSearchResults == null)
            {
                return HttpNotFound();
            }
            return View(flightSearchResults);
        }


        public ActionResult Create()
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }

            return View();
        }
        [HttpPost]
        public JsonResult GetCityData(string Prefix)
        {

            var GetCityData = (from c in db.CityList
                               where c.CityName.StartsWith(Prefix) || c.AirportName.StartsWith(Prefix) || c.AirportCode.StartsWith(Prefix)
                               select new
                               {
                                   c.AirportName,
                                   c.AirportCode,
                                   c.CityName

                               }).ToList();



            if (GetCityData != null)
            {
                //  var BindInf = GetCityData.
            }


            return Json(GetCityData, JsonRequestBehavior.AllowGet);
        }
        // POST: FlightSearchResults/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FlightSearchResults flightSearchResults)
        {
            string createdId = string.Empty;
            try
            {
                // if (ModelState.IsValid)999
                // {
                //yyyy-MM-dd

                string stdate = flightSearchResults.Departure_Date.Split('/')[2] + "/" + flightSearchResults.Departure_Date.Split('/')[1] + "/" + flightSearchResults.Departure_Date.Split('/')[0];

                string enddate = flightSearchResults.Arrival_Date.Split('/')[2] + "/" + flightSearchResults.Arrival_Date.Split('/')[1] + "/" + flightSearchResults.Arrival_Date.Split('/')[0];
                DateTime fromDate = Convert.ToDateTime(stdate);//21/01/2021
                DateTime toDate = Convert.ToDateTime(enddate);

                int totaldays = (toDate - fromDate).Days;
                DateTime tempDateTime = fromDate;

                string[] splited = flightSearchResults.OrgDestFrom.Split(',');
                string[] splited2 = flightSearchResults.OrgDestTo.Split(',');
                string[] splited3 = flightSearchResults.AirLineName.Split(',');
                string[] splited4 = new string[3];
                if (flightSearchResults.Triptype == "R")
                {
                    splited4 = flightSearchResults.Rt_AirLineName.Split(',');
                }

                for (int i = 1; i <= (totaldays + 1); i++)
                {
                    if (i != 1)
                    {
                        tempDateTime = tempDateTime.AddDays(1);
                        flightSearchResults.Departure_Date = tempDateTime.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        flightSearchResults.Departure_Date = tempDateTime.ToString("dd/MM/yyyy");
                    }

                    decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                    flightSearchResults.OrgDestFrom = splited[0] + "," + splited[2];
                    flightSearchResults.OrgDestTo = splited2[0] + "," + splited2[2];
                    flightSearchResults.AirLineName = splited3[0];

                    if (flightSearchResults.Triptype == "R")
                    {
                        flightSearchResults.Rt_AirLineName = splited4[0];
                    }

                    flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);
                    //flightSearchResults.DepAirportCode = flightSearchResults.OrgDestFrom;
                    flightSearchResults.DepAirportCode = splited[2];

                    //flightSearchResults.DepartureCityName = flightSearchResults.OrgDestFrom;
                    flightSearchResults.DepartureCityName = splited[0];

                    //flightSearchResults.DepartureAirportName = flightSearchResults.OrgDestFrom;
                    flightSearchResults.DepartureAirportName = splited[1];

                    //flightSearchResults.ArrivalCityName = flightSearchResults.OrgDestTo;
                    flightSearchResults.ArrivalCityName = splited2[0];

                    //flightSearchResults.ArrAirportCode = flightSearchResults.OrgDestTo;
                    flightSearchResults.ArrAirportCode = splited2[2];

                    //flightSearchResults.ArrivalAirportName = flightSearchResults.OrgDestTo;
                    flightSearchResults.ArrivalAirportName = splited2[1];

                    flightSearchResults.MarketingCarrier = splited3[1];
                    flightSearchResults.valid_Till = toDate;
                    flightSearchResults.RBD = "0";
                    flightSearchResults.Total_Seats = flightSearchResults.Avl_Seat;
                    flightSearchResults.Used_Seat = "0";
                    flightSearchResults.Basicfare = 0;
                    flightSearchResults.YQ = 0;
                    flightSearchResults.YR = 0;
                    flightSearchResults.WO = 0;
                    flightSearchResults.OT = 0;
                    flightSearchResults.GROSS_Total = 0;
                    flightSearchResults.Markup_Type = "Flat";
                    flightSearchResults.Admin_Markup = 0;
                    flightSearchResults.Markup = 0;
                    flightSearchResults.Avl_Seat = "0";
                    flightSearchResults.fareBasis = "";
                    flightSearchResults.Grand_Total = 0;
                    flightSearchResults.FixedDepStatus = true;
                    flightSearchResults.CreatedByUserid = Session["userid"].ToString();
                    db.FlightSearchResults.Add(flightSearchResults);
                    db.SaveChanges();

                    int createid = flightSearchResults.id;

                    createdId = createdId + (!string.IsNullOrEmpty(createdId) ? ("," + createid) : createid.ToString());

                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter();
                    string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                    SqlConnection con = new SqlConnection(constr);
                    SqlCommand cmd = new SqlCommand("FixDepatureCreate");
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@createid", createid);
                    adp.SelectCommand = cmd;
                    int temp = adp.Fill(ds);
                    cmd.Dispose();
                }
                TempData["Msg"] = 1;
                // }
            }
            catch (Exception ex)
            {
                TempData["errorMsg"] = "Create_" + ex.Message + "_" + flightSearchResults.Departure_Date + "_" + flightSearchResults.Arrival_Date;

            }
            return RedirectToAction("PNRUpdate", new { ids = createdId });
        }

        public ActionResult PNRUpdate(string ids)
        {
            List<FlightSearchResults> stdList = new List<FlightSearchResults>();
            try
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    string[] createdIds = ids.Split(',');
                    List<int> intIds = new List<int>();
                    foreach (var item in createdIds)
                    {
                        intIds.Add(Convert.ToInt32(item));
                    }

                    stdList = db.FlightSearchResults.Where(p => intIds.Contains(p.id)).ToList(); //.Where(p => ids.Contains(p.id));
                }
            }
            catch (Exception ex)
            {
                TempData["errorMsg"] = "PNRUpdate_" + ex.Message;
            }

            return View(stdList);
        }
        // GET: FlightSearchResults/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            if (flightSearchResults == null)
            {
                return HttpNotFound();
            }
            return View(flightSearchResults);
        }

        // POST: FlightSearchResults/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FlightSearchResults flightSearchResults)
        {
            try
            {
                //  if (ModelState.IsValid)
                // {
                decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                string[] splitedData = flightSearchResults.OrgDestFrom.Split(',');
                flightSearchResults.OrgDestFrom = splitedData[0] + "," + splitedData[1];

                string[] splitedData2 = flightSearchResults.OrgDestTo.Split(',');
                flightSearchResults.OrgDestTo = splitedData2[0] + "," + splitedData2[1];

                string[] splitedData3 = flightSearchResults.AirLineName.Split(',');

                flightSearchResults.AirLineName = splitedData3[0];


                flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);


                flightSearchResults.DepAirportCode = flightSearchResults.OrgDestFrom;
                flightSearchResults.DepAirportCode = splitedData[1];

                flightSearchResults.DepartureCityName = flightSearchResults.OrgDestFrom;
                flightSearchResults.DepartureCityName = splitedData[0];


                flightSearchResults.DepartureAirportName = flightSearchResults.DepartureAirportName;


                flightSearchResults.ArrivalCityName = flightSearchResults.OrgDestTo;
                flightSearchResults.ArrivalCityName = splitedData2[0];

                flightSearchResults.ArrAirportCode = flightSearchResults.OrgDestTo;
                flightSearchResults.ArrAirportCode = splitedData2[1];
                flightSearchResults.Total_Seats = flightSearchResults.Avl_Seat;
                flightSearchResults.ArrivalAirportName = flightSearchResults.ArrivalAirportName;
                flightSearchResults.FixedDepStatus = true;
                flightSearchResults.MarketingCarrier = flightSearchResults.MarketingCarrier;

                flightSearchResults.CreatedByUserid = Session["userid"].ToString();
                db.Entry(flightSearchResults).State = EntityState.Modified;
                db.SaveChanges();
                TempData["Msg"] = 2;
                //   }

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_Id"))
                    TempData["Msg"] = 0;
            }
            return RedirectToAction("Index");

        }

        // GET: FlightSearchResults/Delete/5
        public ActionResult Delete(int? id)
        {
            FlightSearchResults flightSearchResults = null;

            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }
            else
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                flightSearchResults = db.FlightSearchResults.Find(id);
                if (flightSearchResults == null)
                {
                    return HttpNotFound();
                }

            }



            return View(flightSearchResults);
        }

        // POST: FlightSearchResults/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            db.FlightSearchResults.Remove(flightSearchResults);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult Connect(int? id)
        {
            if (Convert.ToString(Session["userid"]) == "")
            {
                Response.Redirect("/");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FlightSearchResults flightSearchResults = db.FlightSearchResults.Find(id);
            if (flightSearchResults == null)
            {
                return HttpNotFound();
            }
            return View(flightSearchResults);
        }


        // POST: FlightSearchResults/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Connect(FlightSearchResults flightSearchResults)
        {
            try
            {
                //  if (ModelState.IsValid)
                // {
                //decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                //string[] splitedData = flightSearchResults.OrgDestFrom.Split(',');
                //flightSearchResults.OrgDestFrom = splitedData[0] + "," + splitedData[1];

                //string[] splitedData2 = flightSearchResults.OrgDestTo.Split(',');
                //flightSearchResults.OrgDestTo = splitedData2[0] + "," + splitedData2[1];

                //string[] splitedData3 = flightSearchResults.AirLineName.Split(',');

                //flightSearchResults.AirLineName = splitedData3[0];


                //flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);


                //flightSearchResults.DepAirportCode = flightSearchResults.OrgDestFrom;
                //flightSearchResults.DepAirportCode = splitedData[1];

                //flightSearchResults.DepartureCityName = flightSearchResults.OrgDestFrom;
                //flightSearchResults.DepartureCityName = splitedData[0];


                //flightSearchResults.DepartureAirportName = flightSearchResults.DepartureAirportName;


                //flightSearchResults.ArrivalCityName = flightSearchResults.OrgDestTo;
                //flightSearchResults.ArrivalCityName = splitedData2[0];

                //flightSearchResults.ArrAirportCode = flightSearchResults.OrgDestTo;
                //flightSearchResults.ArrAirportCode = splitedData2[1];

                //flightSearchResults.ArrivalAirportName = flightSearchResults.ArrivalAirportName;

                //flightSearchResults.MarketingCarrier = flightSearchResults.MarketingCarrier;

                //// db.Entry(flightSearchResults).State = EntityState.Modified;
                //db.FlightSearchResults.Add(flightSearchResults);
                //db.SaveChanges();

                string createdId = string.Empty;
                int existid = flightSearchResults.id;


                decimal gtotal = Math.Floor(Convert.ToDecimal(flightSearchResults.Grand_Total));
                string[] splitedData = flightSearchResults.OrgDestFrom.Split(',');
                flightSearchResults.OrgDestFrom = splitedData[0] + "," + splitedData[2];

                string[] splitedData2 = flightSearchResults.OrgDestTo.Split(',');
                flightSearchResults.OrgDestTo = splitedData2[0] + "," + splitedData2[2];

                string[] splitedData3 = flightSearchResults.AirLineName.Split(',');

                flightSearchResults.AirLineName = splitedData3[0];
                flightSearchResults.Grand_Total = Convert.ToDouble(gtotal);
                flightSearchResults.DepAirportCode = splitedData[2];
                flightSearchResults.DepartureCityName = splitedData[0];
                flightSearchResults.DepartureAirportName = splitedData[1];
                flightSearchResults.ArrivalCityName = splitedData2[0];
                flightSearchResults.ArrAirportCode = splitedData2[2];
                flightSearchResults.ArrivalAirportName = splitedData2[1];
                flightSearchResults.CreatedByUserid = Session["userid"].ToString();
                flightSearchResults.FixedDepStatus = true;
                db.FlightSearchResults.Add(flightSearchResults);
                db.SaveChanges();

                int newid = flightSearchResults.id;

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                SqlDataAdapter adp = new SqlDataAdapter();
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand("FixDepatureConnect");
                cmd.Connection = con;
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@existid", existid);
                cmd.Parameters.AddWithValue("@newid", newid);
                adp.SelectCommand = cmd;
                int temp = adp.Fill(ds);
                cmd.Dispose();

                // TempData["Msg"] = 2;
                //     }

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_Id"))
                    TempData["Msg"] = 0;
            }
            return RedirectToAction("Index");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class PNR
        {
            public string PNRValue { get; set; }
            public string PNRId { get; set; }
            public string SeatAvail { get; set; }
            public string Amount { get; set; }

        }

        public class AgencyBalance
        {
            public string AgencyName { get; set; }
            public decimal Balance { get; set; }
        }

        public static bool UpdateRecordIntoAnyTable(string tablename, string fieldswithvalue, string wherecondition)
        {
            try
            {
                string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                SqlConnection con = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand("ups_UpdateRecordIntoAnyTable", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("TableName", tablename);
                cmd.Parameters.AddWithValue("FieldsWithValue", fieldswithvalue);
                cmd.Parameters.AddWithValue("WhereCondition", wherecondition);
                con.Open();
                int isSuccess = cmd.ExecuteNonQuery();
                con.Close();
                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public JsonResult PNRInserter(List<PNR> PNRList)
        {
            string result = "";
            for (int i = 0; i < PNRList.Count(); i++)
            {

                string fieldswithvalue = "fareBasis = '" + PNRList[i].PNRValue + "' ,Avl_Seat = '" + PNRList[i].SeatAvail + "',Total_Seats = '" + PNRList[i].SeatAvail + "',Grand_Total=" + PNRList[i].Amount + "";
                string tablename = "FlightSearchResults";
                string wherecondition = "Id = " + PNRList[i].PNRId;

                bool IsSuccess = UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition);
                if (IsSuccess) { result = "Success"; }
            }
            return Json(result);
        }

        public JsonResult Getsuppliorbalance()
        {
            List<object> GetgencyData = new List<object>();
            string userid = Session["userid"].ToString();
            if (!string.IsNullOrEmpty(userid))
            {
                DataTable dtbalance = AccountHelper.GetSupliorbalance(userid);
                if (dtbalance.Rows.Count > 0)
                {
                    string AgencyName = !string.IsNullOrEmpty(dtbalance.Rows[0]["Agency_Name"].ToString()) ? dtbalance.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    decimal Balance = !string.IsNullOrEmpty(dtbalance.Rows[0]["Crd_Limit"].ToString()) ? Convert.ToDecimal(dtbalance.Rows[0]["Crd_Limit"].ToString()) : 0;

                    GetgencyData.Add(AgencyName);
                    GetgencyData.Add(Balance);
                }
            }
            return Json(GetgencyData);
        }

    }
}
