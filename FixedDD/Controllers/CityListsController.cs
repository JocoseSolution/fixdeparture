﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FixedDD.Models;

namespace FixedDD.Controllers
{
    public class CityListsController : Controller
    {
        private myAmdDB db = new myAmdDB();

        // GET: CityLists
        public ActionResult Index(int? page, string search)
        {
            IQueryable<CityList> stdList;
            int totalskip;
            GetIndex(page, search, out stdList, out totalskip);
            return View(stdList.Skip(totalskip).Take(20));
        }
        public PartialViewResult NextIndex(int? page, string search)
        {
            IQueryable<CityList> stdList;
            int totalskip;
            GetIndex(page, search, out stdList, out totalskip);
            return PartialView(stdList.Skip(totalskip).Take(20));
        }
        private void GetIndex(int? page, string search, out IQueryable<CityList> stdList, out int totalskip)
        {
            int pageno = page ?? 1;
            totalskip = (pageno - 1) * 20;
            if (string.IsNullOrEmpty(search))
                stdList = db.CityList.OrderBy(b => b.id);
            else
                stdList = db.CityList.Where(b => b.AirportCode.Contains(search)).OrderBy(b => b.id);
            ViewBag.totalrecord = stdList.Count();
        }
        // GET: CityLists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityList cityList = db.CityList.Find(id);
            if (cityList == null)
            {
                return HttpNotFound();
            }
            return View(cityList);
        }

        // GET: CityLists/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CityLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,AirportCode,AirportName,CityName,CountryName,CountryCode,Latitude,Longitude,WorldAreaCode,Counter,orderseq")] CityList cityList)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.CityList.Add(cityList);
                    db.SaveChanges();
                    TempData["Msg"] = 1;
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Duplicate_Id"))
                    TempData["Msg"] = 0;
            }
            return RedirectToAction("Index");
        }

        // GET: CityLists/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityList cityList = db.CityList.Find(id);
            if (cityList == null)
            {
                return HttpNotFound();
            }
            return View(cityList);
        }

        // POST: CityLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,AirportCode,AirportName,CityName,CountryName,CountryCode,Latitude,Longitude,WorldAreaCode,Counter,orderseq")] CityList cityList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cityList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cityList);
        }

        // GET: CityLists/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CityList cityList = db.CityList.Find(id);
            if (cityList == null)
            {
                return HttpNotFound();
            }
            return View(cityList);
        }

        // POST: CityLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CityList cityList = db.CityList.Find(id);
            db.CityList.Remove(cityList);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
