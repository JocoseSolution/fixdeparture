﻿$("a.popupclass").fancybox({
    maxWidth: 1200,
    maxHeight: 600,
    padding: 0,
    fitToView: true,
    copenEffect: 'none',
    closeEffect: 'none',
    prevEffect: 'none',
    nextEffect: 'none',
    closeBtn: true,
    helpers: {
        overlay: {
            locked: false
        }
    }
});