﻿var PagingPopupNext = (function () {
    var orgidget = document.getElementById('AddressidGet').value;
    var SubHead = document.getElementById('OrgidGet').value;

    var searchFieldData = document.getElementById('searchStringidGet').value;
    var searchCombo = document.getElementById('currentFilteridGet').value;
    var pageget = document.getElementById('pagingselect').value;
    document.getElementById('FeedDivID').innerHTML = "<h4>Please Wait...</h4>"
    $.get("/TicketBookingMaster/CustomerListWithSubHead", { page: pageget, Addressid: orgidget, Org: SubHead, currentFilter: searchCombo, searchString: searchFieldData, way: 0 }, function (data) {
        document.getElementById('FeedDivID').innerHTML = data;
    });
});

var AddNewRows = (function (way) {
    var lastid = $('#RefundTotalEntryGrid').val();
    if (document.getElementById('TextRefundName' + lastid).value.length > 0) {
        lastid = parseInt(lastid) + 1;
        url = "/Home/AddNewItemInGrid?maxid=" + lastid;
        $.get(url, function (data) {
            $('#RefundProductGridDiv').append(data);
            document.getElementById('TextRefundName' + lastid).innerHTML = document.getElementById('examno').innerHTML;
            $('#RefundTotalEntryGrid').val(parseInt(lastid));
        });
    }
});


var AddNewPreRequestRows = (function (way) {
    var lastid = $('#prereqTotalEntryGrid').val();
    if (document.getElementById('TextprereqName' + lastid).value.length > 0) {
        lastid = parseInt(lastid) + 1;
        url = "/Home/AddNewItemInGridForPreReq?maxid=" + lastid;
        $.get(url, function (data) {
            $('#prereqProductGridDiv').append(data);
            document.getElementById('TextprereqName' + lastid).innerHTML = document.getElementById('Categoryid').innerHTML;
            $('#prereqTotalEntryGrid').val(parseInt(lastid));
        });
    }
});



var GenerateGrid = (function () {
    var qty = document.getElementById('TotalMandatoryExams').value;
    var vid = document.getElementById('Vendorid').value;
    $('#ProductGridDiv').html('<h3 class="text-danger text-center>Please Wait.....</h3>"');
    $.get("/CertificationMasters/GetTable", { row: qty, vid: vid }, function (data) {
        $('#ProductGridDiv').html(data);
    });
});

var InvoiceFeed = (function (way) {
    AddNewRows(way);
});


var PreRequestInvoiceFeed = (function (way) {
    AddNewPreRequestRows(way);
});


var CalculateGrandTotalForInvoice = function () {
    var total = document.getElementById('Total').value;
    var Refund = document.getElementById('Refund').value;
    var GSTType = document.getElementById('GSTType').value;
    var GSTRate = document.getElementById('GSTRate').value;
    if (total.length == 0) {
        total = 0;
    }
    if (Refund.length == 0) {
        Refund = 0;
    }
    if (GSTRate.length == 0) {
        GSTRate = 0;
    }

    var taxamount = ((parseFloat(total) - parseFloat(Refund)) * parseFloat(GSTRate)) / 100;
    if (GSTType.length > 0) {
        if (GSTType == 0) {
            document.getElementById('CGSTPer').value = (document.getElementById('SGSTPer').value = GSTRate / 2).toFixed(2);
            document.getElementById('CGSTAmt').value = (document.getElementById('SGSTAmt').value = taxamount / 2).toFixed(2);
            document.getElementById('IGSTPer').value = 0;
            document.getElementById('IGSTAmt').value = 0;
        }
        else {
            document.getElementById('CGSTPer').value = document.getElementById('SGSTPer').value = 0;
            document.getElementById('CGSTAmt').value = document.getElementById('SGSTAmt').value = 0;
            document.getElementById('IGSTPer').value = parseFloat(GSTRate).toFixed(2);;
            document.getElementById('IGSTAmt').value = parseFloat(taxamount).toFixed(2);
        }

        document.getElementById('GrdTotal').value = ((parseFloat(total) + parseFloat(taxamount)) - parseFloat(Refund)).toFixed(2);
    }
};

var CalculateGrandTotal = function () {
    var total = document.getElementById('Total').value;
    var SCharge = document.getElementById('SCharge').value;
    var TxnFee = document.getElementById('TxnFee').value;
    var ExcessBaggage = document.getElementById('ExcessBaggageCharge').value;
    var SpecialService = document.getElementById('SpecialService').value;
    var InsuranceFee = document.getElementById('InsuranceFee').value;
    var Meal = document.getElementById('MealCharge').value;
    if (total.length == 0) {
        total = 0;
    }
    if (SCharge.length == 0) {
        SCharge = 0;
    }
    if (TxnFee.length == 0) {
        TxnFee = 0;
    }
    if (ExcessBaggage.length == 0) {
        ExcessBaggage = 0;
    }
    if (SpecialService.length == 0) {
        SpecialService = 0;
    }
    if (InsuranceFee.length == 0) {
        InsuranceFee = 0;
    }
    if (Meal.length == 0) {
        Meal = 0;
    }


    var tot = ((parseFloat(total) + parseFloat(SCharge) + parseFloat(TxnFee) + parseFloat(ExcessBaggage) + parseFloat(SpecialService) + parseFloat(InsuranceFee) + parseFloat(Meal)));


    var gst = 0;
    gst = document.getElementById('GSTPer').value;
    if (gst.length == 0) {
        gst = 0;
    }

    var gstamt = 0;
    gstamt = ((tot * gst) / 100);
    document.getElementById('GSTAmt').value = parseFloat(gstamt).toFixed(2);

    var gd = parseFloat(tot) + parseFloat(gstamt)

    var vat = 0;
    vat = document.getElementById('DisPer').value;
    if (vat.length == 0) {
        vat = 0;
    }

    var vatamt = 0;
    vatamt = ((gd * vat) / 100);
    document.getElementById('DisAmt').value = parseFloat(vatamt).toFixed(2);
    var grandtotal = parseFloat(gd) - parseFloat(vatamt);

    document.getElementById('GrandTotal').value = parseFloat(grandtotal).toFixed(2);

}
var CalculateTotalPrice = function (TotalRowCount) {
    CalculateBottomTotal(TotalRowCount, 'TextUnitValue', 'Total');
}

var CalculateTotalPriceForInvoice = function (TotalRowCount) {
    document.getElementById('TextTotalFair' + TotalRowCount).value = (parseFloat(document.getElementById('TextUnitValue' + TotalRowCount).value) * parseFloat(document.getElementById('TotalPassanger' + TotalRowCount).value)) + (parseFloat(document.getElementById('TextTranscationFee' + TotalRowCount).value * parseFloat(document.getElementById('TotalPassanger' + TotalRowCount).value)));
    CalculateBottomTotalForInvoice(TotalRowCount, 'TextTotalFair', 'Total');
}

var CalculateTotalPriceForRefund = function (TotalRowCount) {
    document.getElementById('TextRefundTotalFair' + TotalRowCount).value = (parseFloat(document.getElementById('TextRefundSoldChargee' + TotalRowCount).value) * parseFloat(document.getElementById('TotalRefundPassanger' + TotalRowCount).value)) - (parseFloat(document.getElementById('TextRefundUnitValue' + TotalRowCount).value) * parseFloat(document.getElementById('TotalRefundPassanger' + TotalRowCount).value)) - (parseFloat(document.getElementById('TextRefundTranscationFee' + TotalRowCount).value * parseFloat(document.getElementById('TotalRefundPassanger' + TotalRowCount).value)));
    CalculateBottomTotalForRefund(TotalRowCount, 'TextRefundTotalFair', 'Refund');
}

var CheckCheckboxChecked = function (TotalRowCount, SourceID, DestinationId) {
    try {
        TotalRowCount = parseInt(document.getElementById('TotalEntryGrid').value);
        var TotalValue = 0;
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('CheckBoxCheckd' + i).checked == true) {
                if (document.getElementById(SourceID + i).value.length < 0)
                    document.getElementById(SourceID + i).value = "0";
                TotalValue = TotalValue + parseFloat(document.getElementById(SourceID + i).value);
            }
        }
        document.getElementById(DestinationId).value = TotalValue;

    } catch (err) { }
    try {
        CalculateBottomTotal(TotalRowCount, 'TextUnitValue', 'Total');
    } catch (err) { }


}

var CheckCheckboxCheckedForInvoice = function (TotalRowCount, SourceID, DestinationId) {
    try {
        TotalRowCount = parseInt(document.getElementById('TotalEntryGrid').value);
        var TotalValue = 0;
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('CheckBoxCheckd' + i).checked == true) {
                if (document.getElementById(SourceID + i).value.length < 0)
                    document.getElementById(SourceID + i).value = "0";
                TotalValue = TotalValue + parseFloat(document.getElementById(SourceID + i).value);
            }
        }
        document.getElementById(DestinationId).value = TotalValue;

    } catch (err) { }
    try {
        CalculateBottomTotalForInvoice(TotalRowCount, 'TextTotalFair', 'Total');
    } catch (err) { }


}

var CheckCheckboxCheckedForRefund = function (TotalRowCount, SourceID, DestinationId) {
    try {
        TotalRowCount = parseInt(document.getElementById('RefundTotalEntryGrid').value);
        var TotalValue = 0;
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('RefundCheckBoxCheckd' + i).checked == true) {
                if (document.getElementById(SourceID + i).value.length < 0)
                    document.getElementById(SourceID + i).value = "0";
                TotalValue = TotalValue + parseFloat(document.getElementById(SourceID + i).value);
            }
        }
        document.getElementById(DestinationId).value = TotalValue;

    } catch (err) { }
    try {
        CalculateBottomTotalForRefund(TotalRowCount, 'TextRefundTotalFair', 'Refund');
    } catch (err) { }


}

var CalculateBottomTotal = function (TotalRowCount, SourceID, DestinationId) {
    try {
        TotalRowCount = parseInt(document.getElementById('TotalEntryGrid').value);

        var TotalValue = 0;
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('CheckBoxCheckd' + i).checked == true) {
                if (document.getElementById(SourceID + i).value.length < 0)
                    document.getElementById(SourceID + i).value = "0";
                TotalValue = TotalValue + parseFloat(document.getElementById(SourceID + i).value);

            }
        }
        document.getElementById(DestinationId).value = TotalValue.toFixed(2);

    } catch (err) {
        // alert(err);
    }
    CalculateGrandTotal();
    CalculateLableTotal();
}

var CalculateBottomTotalForRefund = function (TotalRowCount, SourceID, DestinationId) {
    try {
        TotalRowCount = parseInt(document.getElementById('RefundTotalEntryGrid').value);

        var TotalValue = 0;
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('RefundCheckBoxCheckd' + i).checked == true) {
                if (document.getElementById(SourceID + i).value.length < 0)
                    document.getElementById(SourceID + i).value = "0";
                TotalValue = TotalValue + parseFloat(document.getElementById(SourceID + i).value);

            }
        }
        document.getElementById(DestinationId).value = TotalValue.toFixed(2);

    } catch (err) {
        // alert(err);
    }
    CalculateGrandTotalForInvoice();
}

var CalculateBottomTotalForInvoice = function (TotalRowCount, SourceID, DestinationId) {
    try {
        TotalRowCount = parseInt(document.getElementById('TotalEntryGrid').value);

        var TotalValue = 0;
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('CheckBoxCheckd' + i).checked == true) {
                if (document.getElementById(SourceID + i).value.length < 0)
                    document.getElementById(SourceID + i).value = "0";
                TotalValue = TotalValue + parseFloat(document.getElementById(SourceID + i).value);

            }
        }
        document.getElementById(DestinationId).value = TotalValue.toFixed(2);

    } catch (err) {
        // alert(err);
    }
    CalculateGrandTotalForInvoice();
    CalculateLableTotalForInvoice();
}

var CalculateLableTotal = function () {
    TotalRowCount = parseInt(document.getElementById('TotalEntryGrid').value);
    var TotalQty = 0, TotalAmount = 0;

    try {
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('CheckBoxCheckd' + i).checked == true) {
                if (document.getElementById("TextUnitValue" + i).value.length < 0)
                    document.getElementById("TextUnitValue" + i).value = "0";
                TotalAmount = TotalAmount + parseFloat(document.getElementById("TextUnitValue" + i).value);
            }
        }
    } catch (err) {
        TotalAmount = 0;
    }

    document.getElementById("TotalAmount").innerText = TotalAmount.toFixed(2);
    document.getElementById("TotalItemCount").innerText = (TotalRowCount - 1);
}

var CalculateLableTotalForInvoice = function () {
    TotalRowCount = parseInt(document.getElementById('TotalEntryGrid').value);
    var TotalQty = 0, TotalAmount = 0;

    try {
        for (var i = 1; i < TotalRowCount; i++) {
            if (document.getElementById('CheckBoxCheckd' + i).checked == true) {
                if (document.getElementById("TextTotalFair" + i).value.length < 0)
                    document.getElementById("TextTotalFair" + i).value = "0";
                TotalAmount = TotalAmount + parseFloat(document.getElementById("TextTotalFair" + i).value);
            }
        }
    } catch (err) {
        TotalAmount = 0;
    }

    document.getElementById("TotalAmount").innerText = TotalAmount.toFixed(2);
    document.getElementById("TotalItemCount").innerText = (TotalRowCount - 1);
}


var searchstringvalue = "NA"; var RefSOInformationHTML = "";

var SearchProductDetails = function (id) {
    var pagesizeGet = document.getElementById('TotalRecord').value;
    var SearchStringGet = document.getElementById('textBoxModelNo' + id).value;
    $.get("/ProductsMaster/SearchProductDetailsForListBox", { pagesize: pagesizeGet, searchString: SearchStringGet, RowiD: selectedRowId }, function (data) {
        document.getElementById('ListBoxModel' + id).innerHTML = data;
        productInformationHTML = data;
    });
};

var ChangeFY = (function () {
    var fyvalue = document.getElementById('FY').value;
    $.get('/Home/SetChangeFY', { FYVa: fyvalue }, function (data) {
        alert(data);
    });
});

var SearchRefSoDetails = function () {

    var SearchStringGet = document.getElementById('RevesionNo').value;
    $.get("/PurchaseOrders/GetRefSONoDetails", { GlobalsearchString: SearchStringGet }, function (data) {
        document.getElementById('RefSoDetailsDivID').innerHTML = data;
        RefSOInformationHTML = data;
    });
};

var GetPartyDetails = function (NameID, AddressID) {
    var customernameGet = $("#" + NameID + " option:selected").text();
    var AddressGet = $("#" + AddressID + " option:selected").text();
    $.get("/PartyMaster/GetPartyDetails", { custmername: customernameGet, Address: AddressGet }, function (data) {
        var customerdata = JSON.parse(data);
        try {
            document.getElementById('Division').value = customerdata.Division;
        } catch (err) { }
        document.getElementById('EmailID').value = customerdata.Email;
        try {
            document.getElementById('TinNo').value = customerdata.GSTNo;
            document.getElementById('PinCode').value = customerdata.PinCode;
            document.getElementById('ServiceTaxNo').value = customerdata.ServiceTaxNo;
            document.getElementById('EccNo').value = customerdata.EccNo;
            document.getElementById('Colletorate').value = customerdata.Colletorate;
            document.getElementById('Range').value = customerdata.Range;
            document.getElementById('Region').value = customerdata.Region;
            document.getElementById('Vertical').value = customerdata.Vertical;
        } catch (err) { }
        document.getElementById('CountryID').value = customerdata.CountryID;
        document.getElementById('StateID').value = customerdata.StateID;
        document.getElementById('CityID').value = customerdata.CityID;
    });

}

var SearchPartyDetails = function () {
    var SearchStringGet = document.getElementById('textBoxModelNo').value;
    $.get("/ProductsMaster/SearchProductDetailsForListBox", { pagesize: pagesizeGet, searchString: SearchStringGet, RowiD: selectedRowId }, function (data) {
        document.getElementById('ListBoxModel' + id).innerHTML = data;
    });
};
var GetPartyAddress = function (Sourceid, DestID) {
    var code = $("#" + Sourceid + " option:selected").text();
    $.get("/PartyMaster/GetPartyAddress", { custmername: code }, function (data) {
        document.getElementById(DestID).innerHTML = data;
    });
}

var TextBoxModelKeyup = function (e, id) {

    if (document.getElementById('CheckEnter').checked == false)
        SearchProductDetails(id);
    else {
        if (e.keyCode == 13)
            SearchProductDetails(id);
    }
};

var TextBoxRefSoKeyup = function (e, id) {

    if (document.getElementById('CheckEnter').checked == false)
        SearchProductDetails(id);
    else {
        if (e.keyCode == 13)
            SearchProductDetails(id);
    }
};

var ModelListboxDisplayTruFalse = function (id) {
    if (document.getElementById('ListBoxModel' + id).style.display == "block") {
        for (var re = 0; re < TotalEntryInGrid; re = re + 1) {
            document.getElementById('ListBoxModel' + re).innerHTML = "";
            document.getElementById('ListBoxModel' + re).style.display = "none";
        }
        document.getElementById('ListBoxModel' + id).style.display = "none";
    }
    else {
        for (var re = 0; re < TotalEntryInGrid; re = re + 1) {
            document.getElementById('ListBoxModel' + re).innerHTML = "";
            document.getElementById('ListBoxModel' + re).style.display = "none";
        }
        document.getElementById('ListBoxModel' + id).style.display = "block";
        document.getElementById('ListBoxModel' + id).innerHTML = productInformationHTML;
    }

    selectedRowId = id;
};

var TextBoxItemRegionKeyup = function (e, id) {

    if (document.getElementById('CheckEnter').checked == false)
        SearchItemRegionDetails(id);
    else {
        if (e.keyCode == 13)
            SearchItemRegionDetails(id);
    }
};

var SetAccountManager = (function () {
    var url = '/Home/GetAccountManager?regionhead=' + $('#RegionHead').val();
    $.get(url, function (data) {
        document.getElementById('AccountManager').innerHTML = data;
    });
});

var FoucsItemRegionGrid = function (id) {
    var UniqueFoucsid = 'snoItemRegion1';
    document.getElementById('snoItemRegion1').focus();
    selectedItemRegionRowId = id;
    FocusIdItemRegion = 1;
};

var SearchItemRegionDetails = function (id) {
    var pagesizeGet = document.getElementById('TotalRecord').value;
    var SearchStringGet = document.getElementById('ItemRegion' + id).value;
    $.get("/PurchaseOrders/SearchItemFRegionDetailsForListBox", { pagesize: pagesizeGet, searchString: SearchStringGet, RowiD: selectedRowId }, function (data) {
        document.getElementById('ListBoxItemRegion' + id).innerHTML = data;
        ItemRegionInformationHTML = data;
    });
};

var ItemRegionListboxDisplayTruFalse = function (id) {
    if (document.getElementById('ListBoxItemRegion' + id).style.display == "block") {
        for (var re = 0; re < TotalEntryInGrid; re = re + 1) {
            document.getElementById('ListBoxItemRegion' + re).innerHTML = "";
            document.getElementById('ListBoxItemRegion' + re).style.display = "none";
        }
        document.getElementById('ListBoxItemRegion' + id).style.display = "none";
    }
    else {
        for (var re = 0; re < TotalEntryInGrid; re = re + 1) {
            document.getElementById('ListBoxItemRegion' + re).innerHTML = "";
            document.getElementById('ListBoxItemRegion' + re).style.display = "none";
        }
        document.getElementById('ListBoxItemRegion' + id).style.display = "block";
        document.getElementById('ListBoxItemRegion' + id).innerHTML = ItemRegionInformationHTML;
    }

    selectedRowId = id;
};

var HideItemRegionGridEntryGrid = function (e) {

    if (e.keyCode == 13) { //On Enter
        document.getElementById('ListBoxItemRegion' + selectedRowId).style.display = 'none';
        document.getElementById('TextboxExciseAmt' + selectedRowId).focus();
    }
    else if (e.keyCode == 40) {
        var FocusMaxID = parseInt(document.getElementById('MaxHiddenProductIDForItemRegion').value);

        if (FocusId < FocusMaxID) {
            FocusId = parseInt(FocusId) + 1;

        }
        else {
            FocusId = 1;
        }
        document.getElementById('snoItemRegion' + FocusId).focus();
    }
    else if (e.keyCode == 38) {
        if (FocusId > 1) {
            FocusId = parseInt(FocusId) - 1;
            document.getElementById('snoItemRegion' + FocusId).focus();
        } else {
            document.getElementById('ItemRegion' + selectedRowId).focus();
        }

    }

};

var CopyItemRegionGridToText = function (id, way) {

    document.getElementById('ItemRegion' + selectedRowId).value = (document.getElementById('tdItemRegion' + id).innerText);

    //TextBusinessUnit

    // Removing Blank Space
    document.getElementById('ItemRegion' + selectedRowId).value = document.getElementById('ItemRegion' + selectedRowId).value.trim();

    if (way == 1) {
        document.getElementById('ListBoxItemRegion' + selectedRowId).style.display = "none";

        document.getElementById('TextboxExciseAmt' + selectedRowId).focus();

    }
};

var PartyListboxDisplayTruFalse = function () {
    if (document.getElementById('customerlistDivID').style.display == "block")
        document.getElementById('customerlistDivID').style.display = "none";
    else
        document.getElementById('customerlistDivID').style.display = "block";

};

var FoucsGrid = function (id) {
    var UniqueFoucsid = 'sno1';
    document.getElementById('sno1').focus();
    selectedRowId = id;
    FocusId = 1;
};

var FocusId = 1, FocusIdItemRegion = 1;

var HideEntryGrid = function (e) {

    if (e.keyCode == 13) { //On Enter
        document.getElementById('ListBoxModel' + selectedRowId).style.display = 'none';
        document.getElementById('TextboxUnitValue' + selectedRowId).focus();
        NewEntyInGrid(parseInt(selectedRowId + 1));
    }
    else if (e.keyCode == 40) {
        var FocusMaxID = parseInt(document.getElementById('MaxHiddenProductID').value);

        if (FocusId < FocusMaxID) {
            FocusId = parseInt(FocusId) + 1;

        }
        else {
            FocusId = 1;
        }
        document.getElementById('sno' + FocusId).focus();
    }
    else if (e.keyCode == 38) {
        if (FocusId > 1) {
            FocusId = parseInt(FocusId) - 1;
            document.getElementById('sno' + FocusId).focus();
        } else {
            document.getElementById('textBoxModelNo' + selectedRowId).focus();
        }

    }

};

var disableEnterGlobal = function (e) {
    if (e.keyCode == 13)
        return false;
};

var CheckControl = (function (ControlName) {
    try {

        var Value = document.getElementById(ControlName).value;
        return true;

    } catch (err) {
        return false;
    }
});

var CopyGridToText = function (id, way) {

    document.getElementById('textBoxModelNo' + selectedRowId).value = (document.getElementById('tdModel' + id).innerText);
    document.getElementById('TextboxBrandName' + selectedRowId).value = (document.getElementById('tdbrand' + id).innerText);

    document.getElementById('TextboxProductLine' + selectedRowId).value = (document.getElementById('tdProduct_Line' + id).innerText);
    document.getElementById('TextboxUnitValue' + selectedRowId).value = (document.getElementById('tdMRP' + id).innerText);

    document.getElementById('TextBusinessUnit' + selectedRowId).value = (document.getElementById('tdBusniessUnit' + id).innerText);

    document.getElementById('TextboxAvailbleStock' + selectedRowId).value = (document.getElementById('tdStock' + id).innerText);

    //TextBusinessUnit

    // Removing Blank Space
    document.getElementById('textBoxModelNo' + selectedRowId).value = document.getElementById('textBoxModelNo' + selectedRowId).value.trim();
    document.getElementById('TextboxBrandName' + selectedRowId).value = document.getElementById('TextboxBrandName' + selectedRowId).value.trim();

    document.getElementById('TextboxProductLine' + selectedRowId).value = document.getElementById('TextboxProductLine' + selectedRowId).value.trim();
    document.getElementById('TextboxUnitValue' + selectedRowId).value = document.getElementById('TextboxUnitValue' + selectedRowId).value.trim();

    document.getElementById('TextBusinessUnit' + selectedRowId).value = document.getElementById('TextBusinessUnit' + selectedRowId).value.trim();
    document.getElementById('TextboxAvailbleStock' + selectedRowId).value = document.getElementById('TextboxAvailbleStock' + selectedRowId).value.trim();


    if (CheckControl('TextboxSerialize' + selectedRowId) == true) {

        document.getElementById('TextboxSerialize' + selectedRowId).value = (document.getElementById('tdProductSerialNo' + id).innerText);
        document.getElementById('TextboxSerializebtn' + selectedRowId).value = (document.getElementById('tdProductSerialNo' + id).innerText);

        document.getElementById('TextboxSerialize' + selectedRowId).value = document.getElementById('TextboxSerialize' + selectedRowId).value.trim();
        document.getElementById('TextboxSerializebtn' + selectedRowId).value = document.getElementById('TextboxSerializebtn' + selectedRowId).value.trim();
    }


    if (way == 1) {
        document.getElementById('ListBoxModel' + selectedRowId).style.display = "none";

        document.getElementById('TextboxUnitValue' + selectedRowId).focus();
        NewEntyInGrid(parseInt(selectedRowId + 1));
    }
};

var SaveLastEntryOfGrid = function (idvalue) {
    $.get("/Home/SaveLastEntryOfGrid", { id: idvalue }, function (data) {

    });
};

var CalculateTotalPriceForEntry = function (RowId) {
    //,'TextboxUnitValue','TextboxQTY','TextboxTotalPrice'
    document.getElementById('TextboxTotalPrice' + RowId).value = parseFloat(document.getElementById('TextboxUnitValue' + RowId).value) * parseFloat(document.getElementById('TextboxQTY' + RowId).value);
    CalculateTotalValueForBottomTextBoxForEntry();
    CalculateTotalQTYForBottomLabelForEntry();
    CalculateExciseAountForBottomTextBoxForEntry();

};

var TotalPriceCalculationForRepairWithToAndFrom = (function (textid) {

    try {

        var f = document.getElementById('TextboxUnitValue' + textid).value;
        // alert(f);
        var ToAndFroChargGet = document.getElementById('TextboxToAndFroForRepair' + textid).value;
        // alert(ToAndFroChargGet);
        var TextboxLoadingGet = document.getElementById('TextboxLodingAndBoardingForRepair' + textid).value;
        //alert(TextboxLoadingGet);

        //alert(TextboxAnyChargesGet);
        var s = document.getElementById('TextboxQTY' + textid).value;
        // alert(s);
        if (f == "")
            f = ("0");
        if (ToAndFroChargGet == "")
            ToAndFroChargGet = ("0");
        if (TextboxLoadingGet == "")
            TextboxLoadingGet = ("0");

        if (s == "")
            s = ("0");

        var TotalAmount = parseFloat(f) + parseFloat(ToAndFroChargGet) + parseFloat(TextboxLoadingGet);


        var mul = parseFloat(s) * parseFloat(TotalAmount);
        document.getElementById('TextboxTotalPrice' + textid).value = (mul.toString());
        CalculateTotalValueForBottomTextBoxForEntry();
        CalculateTotalQTYForBottomLabelForEntry();

    } catch (err) {
        //   
    }

});

var EngineerTotalPriceCalculation = (function (textid) {

    try {

        var f = document.getElementById('TextboxCharges' + textid).value;
        // alert(f);
        var ToAndFroChargGet = document.getElementById('TextboxToAndFro' + textid).value;
        // alert(ToAndFroChargGet);
        var TextboxLoadingGet = document.getElementById('TextboxLodingAndBoarding' + textid).value;
        //alert(TextboxLoadingGet);
        var TextboxAnyChargesGet = 0;
        try {
            TextboxAnyChargesGet = document.getElementById('TextboxOtherCharges' + textid).value;
        } catch (err) {
            TextboxAnyChargesGet = 0;
        }
        //alert(TextboxAnyChargesGet);
        var s = document.getElementById('TextboxDuration' + textid).value;
        // alert(s);
        if (f == "")
            f = ("0");
        if (ToAndFroChargGet == "")
            ToAndFroChargGet = ("0");
        if (TextboxLoadingGet == "")
            TextboxLoadingGet = ("0");
        if (TextboxAnyChargesGet == "")
            TextboxAnyChargesGet = ("0");
        if (s == "")
            s = ("0");

        var TotalAmount = parseFloat(f) + parseFloat(ToAndFroChargGet) + parseFloat(TextboxLoadingGet) + parseFloat(TextboxAnyChargesGet);


        var mul = parseFloat(s) * parseFloat(TotalAmount);
        document.getElementById('TextboxTotalValue' + textid).value = (mul.toString());
        CalculateTotalValueForBottomTextBoxForEngineer();
        CalculateTotalQTYForBottomLabelForEngineer();

    } catch (err) {
        //   
    }

});

var CalculateTotalValueForBottomTextBoxForEngineer = (function () {

    var TotalAmount = 0;
    for (i = 0; i < (TotalRecordForEngineer); i++) {
        if (document.getElementById('DivIDForEngineer' + i).style.display != 'none') {
            TotalAmount = parseFloat(TotalAmount) + parseFloat(document.getElementById('TextboxTotalValue' + i).value);
        }
    }
    $('#TotalAmount').html(TotalAmount);
    try {
        $('#Total').val(TotalAmount);
    } catch (err) { }
});

var CalculateTotalQTYForBottomLabelForEngineer = (function () {

    var TotalExciseAmount = 0;
    for (i = 0; i < (TotalRecordForEngineer); i++) {
        if (document.getElementById('DivIDForEngineer' + i).style.display != 'none') {
            var ExciseAmount = 0;
            ExciseAmount = document.getElementById('TextboxDuration' + i).value;
            if (ExciseAmount == "")
                ExciseAmount = "0";

            TotalExciseAmount = parseFloat(TotalExciseAmount) + parseFloat(ExciseAmount);
        }
    }
    $('#GrandTotalQty').html(TotalExciseAmount);

});

var CalculateTotalValueForBottomTextBoxForEntry = (function () {
    try {
        var TotalAmount = 0;
        for (i = 0; i <= (TotalRecord); i++) {
            try {
                if (document.getElementById('DivID' + i).style.display != 'none') {
                    TotalAmount = parseFloat(TotalAmount) + parseFloat(document.getElementById('TextboxTotalPrice' + i).value);
                }
            } catch (err) {
                //alert(err);
            }
        }
        $('#TotalAmount').html(TotalAmount.toString());
        try {
            $('#Total').val(TotalAmount.toString());
        } catch (err) { }
    } catch (err) {
        //alert(err);
    }
});

var CalculateTotalQTYForBottomLabelForEntry = (function () {
    var TotalExciseAmount = 0;
    for (i = 0; i <= (TotalRecord); i++) {
        try {
            if (document.getElementById('DivID' + i).style.display != 'none') {
                TotalExciseAmount = parseFloat(TotalExciseAmount) + parseFloat(document.getElementById('TextboxQTY' + i).value);
            }

        } catch (err) {
            // alert(err);
        }
    }

    document.getElementById('GrandTotalQty').innerText = TotalExciseAmount;

});

var CalculateExciseAountForBottomTextBoxForEntry = (function () {
    try {
        var TotalExciseAmount = 0;
        for (i = 0; i < (TotalRecord); i++) {
            try {
                if (document.getElementById('DivID' + i).style.display != 'none') {
                    var ExciseAmount = 0;
                    ExciseAmount = document.getElementById('TextboxExciseAmt' + i).value;
                    if (ExciseAmount == "")
                        ExciseAmount = "0";

                    TotalExciseAmount = parseFloat(TotalExciseAmount) + parseFloat(ExciseAmount);
                }
            } catch (err) { }
        }
        document.getElementById('TotalExcsieAmount').value = TotalExciseAmount;
    } catch (err) { }
});

var DeleteDataForEntry = (function (textid) {
    if (document.getElementById('textBoxModelNo' + textid).value != "") {
        if (confirm("Delete Entry No." + (textid + 1) + "  .Are You Sure.") == true) {
            document.getElementById('DivID' + textid).style.display = 'none';
            document.getElementById('textBoxModelNo' + textid).value = '';
            ArrangeSerialnoAfterDelete();

        }
    }

});

var DeleteDataForEntryPickOption = (function (textid) {
    document.getElementById('DivID' + (textid - 1)).style.display = 'none';
    document.getElementById('textBoxModelNo' + (textid - 1)).value = '';
    ArrangeSerialnoAfterDelete();
});

var DeleteDataForEntryForEngineerVisit = (function (textid) {
    if (document.getElementById('textBoxTypeOfService' + textid).value != "") {
        if (confirm("Delete Entry No." + (textid + 1) + "  .Are You Sure.") == true) {
            document.getElementById('DivIDForEngineer' + textid).style.display = 'none';
            document.getElementById('textBoxTypeOfService' + textid).value = '';
            ArrangeSerialnoAfterDeleteForEngineering();

        }
    }

});

var ArrangeSerialnoAfterDelete = (function () {
    var ser = 0;
    for (i = 0; i < (TotalRecord); i++) {
        try {
            if (document.getElementById('DivID' + i).style.display != 'none') {
                ser = ser + 1;
                document.getElementById('textBoxSno' + i).value = ser;
            }
        } catch (err) { }
    }

    $('#TotalItemCount').html((ser - 1));
    CalculateTotalValueForBottomTextBoxForEntry();
    CalculateTotalQTYForBottomLabelForEntry();
    CalculateExciseAountForBottomTextBoxForEntry();
});

var ArrangeSerialnoAfterDeleteForEngineering = (function () {
    var ser = 0;
    for (i = 0; i < (TotalRecordForEngineer); i++) {
        try {
            if (document.getElementById('DivIDForEngineer' + i).style.display != 'none') {
                ser = ser + 1;
                document.getElementById('textBoxSnoForEnginerr' + i).value = ser;
            }
        } catch (err) { }
    }

    $('#TotalItemCount').html((ser - 1));
    CalculateTotalValueForBottomTextBoxForEngineer();
    CalculateTotalQTYForBottomLabelForEngineer();
});

var ArrangeSerialnoAfterEdit = (function () {
    var ser = 0;
    for (i = 0; i < (TotalRecord); i++) {
        try {
            if (document.getElementById('DivID' + i).style.display != 'none') {
                ser = ser + 1;
                document.getElementById('textBoxSno' + i).value = ser;
            }
        } catch (err) { }
    }

    $('#TotalItemCount').html((ser));
    CalculateTotalValueForBottomTextBoxForEntry();
    CalculateTotalQTYForBottomLabelForEntry();
});

var ArrangeSerialnoAfterEditForEngineerVisit = (function () {
    var ser = 0;
    for (i = 0; i < (TotalRecordForEngineer); i++) {
        try {
            if (document.getElementById('DivIDForEngineer' + i).style.display != 'none') {
                ser = ser + 1;
                document.getElementById('textBoxSnoForEnginerr' + i).value = ser;
            }
        } catch (err) { }
    }

    $('#TotalItemCount').html((ser));
    CalculateTotalValueForBottomTextBoxForEngineer();
    CalculateTotalQTYForBottomLabelForEngineer();
});

var GetLastFiveQuotationDetials = function (textid) {
    if (document.getElementById('PreviewDivID' + textid).style.display == "block")
        document.getElementById('PreviewDivID' + textid).style.display = "none";
    else
        document.getElementById('PreviewDivID' + textid).style.display = "block";
    var ModelNoGet1 = document.getElementById('textBoxModelNo' + textid).value;
    var CustomerNameGet1 = document.getElementById('CustomerName').value;

    try {

        $.get("/Quotation/LastFiveQuotationDetails", { ModelNoGet: ModelNoGet1, PartyNameGet: CustomerNameGet1 }, function (data) {
            document.getElementById('PreviewDivID' + textid).innerHTML = data;
        });
    } catch (err) {
        //alert(err);
    }
};